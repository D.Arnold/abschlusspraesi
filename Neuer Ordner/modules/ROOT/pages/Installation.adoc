= Installation Antora

---
Zur Nutzung von Antora auf einem Windows System ist Node erforderlich. Ob sich Node bereits auf dem Rechner befindet kann mit dem Befehl node –version in der PowerShell getestet werden. Ist dies nicht der Fall, wird zur Installation von Node zunächst de Windows Paket Manager Chocolatey benötigt. Hierfür ist die PowerShell als Administrator auszuführen und der folgenden Befehl einzugeben:

----
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object Sys-tem.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
----
Anschließed kann Node installiert werden
----
choco install -y nvm
----

Die PowerShell muss anschließend neu gestartet werden. Mit nvm install 12.16.2 wird Node installiert. Alternativ kann die neuste Node Version direkt installiert werden.
----
choco install -y nodejs-lts
----
Dann kann Antora installiert werden.
----
npm i -g @antora/cli@2.3 @antora/site-generator-default@2.3
----
Dieser Befehl installiert Antora Global. Mit antora -v kann die aktuelle Version von Antora angezeigt werden.
